# HTTP API infrastructure on AWS

## GOAL

Design a cloud infrastructure ready for production (24/7) with fluctuant and heavy loads. This platform have to handle http call serve by node.js stateless program. No legacy consideration.

## Method

There are several ways to adress this goal.


I will describe two of it:

__Full AWS managed services__
 
*pros:*

 - Less setup and maintain cost
 - Heavily scalable
 - Pay as you use
 - Follow AWS best practice

*cons:*

 - Cloud provider lock-in
 - Hard to estimate cost

__Hybrid platform (replace some elements by opensource own managed solution)__
 
*pros:*

 - More cost control over resources
 - Less cloud provider lock-in
 - More tool choice

*cons:*

 - Higher setup and maintain cost
 - Harder to find talent

> Enough words, let's design.

# Full AWS managed services

![](docs/images/fullAwsManagedServices.png)

And with some usefull notes :

![](docs/images/fullAwsManagedServicesWithNotes.png)

# Hybrid platform

![](docs/images/hybridPlatform.png)

And with some usefull notes :

![](docs/images/hybridPlatformWithNotes.png)

> For better lisibility, I don't represent monitoring tools. I would use Prometheus-Grafana.

# CI/CD pipeline

> I would use Gitlab-ci for testing and deployment pipeline. CI is triggered on pull request (gitflow).
> For the demo I show you only a draft for a full managed services solution. 
> A platform based on container will replace some of this step by building images and pushing them on docker registry. 
> Then a k8s rollout deployment would be triggered.

![](docs/images/gitlab-ci-pipeline.png)

# Deployment

> I would use Terraform to deploy cloud resources.
> For hybrid platform, I would use ansible (triggered by terraform => local_exec) to configure hosts. 
> EKS cluster would be deployed and centralised through rancher 2. It would be the entrypoint for dev. and no ops user.

# Monitoring

> I would use grafana for dashboard and alerting. 
> In a full AWS managed services, I would use SNS triggered by cloudwatch to send notification.

# Other tools

 - Sonarqube are usefull for code analysis/quality/compliance.
 - Elastic search stack could be usefull for APM, embedded ML, and other features depends on your needs.
 - Many more...



## Annex

Some sources helpfull for your need:

[Gaming platform design](https://d1.awsstatic.com/architecture-diagrams/Asynchronous-Online-Gaming%20-%20Basic.pdf)

[Ads platform design](https://media.amazonwebservices.com/architecturecenter/AWS_ac_ra_adserving_06.pdf)

[Helm chart (incubator) for apache Kafka](https://github.com/helm/charts/tree/master/incubator/kafka)



